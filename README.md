# useR! 2021 art gallery

This is the template for the slideshow exhibited at [useR!2021](https://user2021.r-project.org/):

https://saramortara.gitlab.io/art_template_user2021/#1

The script that that generates the labels (`scripts/01_generate_label.R`) must be run before compiling the Rmd.
